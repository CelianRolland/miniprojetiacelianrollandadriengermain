import copy
from math import log, sqrt, inf
from random import choice

import numpy as np
from rich.console import Console
from rich.progress import track
from rich.table import Table


class Node(object):
    def __init__(self, logic, board, move=(None, None), wins=0, visits=0, children=None):
        # Save the #wins:#visited ratio
        self.state = board
        self.move = move
        self.wins = wins
        self.visits = visits
        self.children = children or []
        self.parent = None
        self.untried_moves = logic.get_possible_moves(board)

    def add_child(self, child):
        child.parent = self
        self.children.append(child)


def traverse(node: tuple, player: int, board: np.ndarray, visited: dict, logic, game_over=False):
    """
        @return   the path of node connecting two borders for player, if existing
        """
    (x, y) = node
    neighbours = logic.get_neighbours(node)
    try:
        if visited[node]:
            pass
    except KeyError:
        if board[x][y] == player:
            visited[node] = 1

            if logic.is_border(node, player):
                game_over = True

            for neighbour in neighbours:
                if traverse(neighbour, player, board, visited,logic,game_over):
                    game_over = True
    if game_over:
            return visited


def game_over(logic, player, state):
    for _ in range(logic.ui.board_size):
        border = (_, 0)
        path = traverse(border, 1, state, {}, logic)
        if path:
            return -1
    for _ in range(logic.ui.board_size):
         border = (0, _)
         path = traverse(border, 2, state, {},logic)
         if path:
            return 1
    return None


def minmax_black(self, logic, node, player,move):
    new_board = copy.deepcopy(node.state)
    old_player = (player % 2) + 1
    new_board[move[0]][move[1]] = old_player
    node.untried_moves = logic.get_possible_moves(new_board)
    res = game_over(logic, old_player, new_board)
    if res:
        return res
    score = dict()
    if player == 1:
        for n in node.untried_moves:
            next_node = Node(self.logic, new_board, move=n)
            score[n] = minmax_white(self, logic, next_node, 2, n)
        min_score = min(score.values())
        return min_score
    if player == 2:
        for n in node.untried_moves:
            next_node = Node(self.logic, new_board, move=n)
            score[n] = minmax_white(self, logic, next_node, 1, n)
        max_score = max(score.values())
        return max_score


def minmax_white(self, logic, node, player,move):
    new_board = copy.deepcopy(node.state)
    old_player = (player % 2) + 1
    new_board[move[0]][move[1]] = old_player
    node.untried_moves = logic.get_possible_moves(new_board)
    res = game_over(logic, old_player, new_board)
    if res:
        return res
    score = dict()
    if player == 2:
        for n in node.untried_moves:
            next_node = Node(self.logic, new_board, move=n)
            score[n] = minmax_white(self, logic, next_node, 1,n)
        return max(score.values())
    if player == 1:
        for n in node.untried_moves:
            next_node = Node(self.logic, new_board, move=n)
            score[n] = minmax_white(self, logic, next_node, 2,n)
        return min(score.values())


class STRAT:
    def __init__(self, logic, ui, board_state, starting_player):
        self.logic = logic
        self.ui = ui
        self.root_state = copy.copy(board_state)
        self.state = copy.copy(board_state)
        self.starting_player = starting_player
        self.players = [1, 2]
        self.players.remove(self.starting_player)
        self.other_player = self.players[0]
        self.turn = {True: self.starting_player, False: self.other_player}
        self.turn_state = True

    def start(self) -> tuple:
        root_node = Node(self.logic, self.root_state)
        score = dict()
        if self.starting_player is self.ui.BLACK_PLAYER:
            for n in root_node.untried_moves:
                next_node = Node(self.logic, root_node.state, move=n)
                score[n] = minmax_black(self, self.logic, next_node, 2, n)
            return min(score, key=score.get)
        elif self.starting_player is self.ui.WHITE_PLAYER:
            for n in root_node.untried_moves:
                next_node = Node(self.logic, root_node.state, move=n)
                score[n] = minmax_white(self, self.logic, next_node, 1,n)
            return max(score, key=score.get)
